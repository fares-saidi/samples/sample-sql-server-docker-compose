#/bin/bash
sed -i "/${pwd}\/restart-db.sh/d" /etc/crontab
echo "*/30 * * * *    $(whoami)    $(pwd)/restart-db.sh $(pwd)" >> /etc/crontab
/etc/init.d/cron reload
