#/bin/bash
cd ${1:-$(pwd)}
docker-compose down || true
rm -rf ./volumes/postgres
docker-compose up -d
