# Sample SQL Server Docker-Compose

Schema and Data obtained from <https://www.sqltutorial.org/sql-sample-database/>

![schema-image](./images/SQL-Sample-Database-Schema.png)

SQL script located under [./data/postgres/create_tables.sql](./data/postgres/create_tables.sql)

## Database refresh

The database content is wiped and reset periodically. Currently set up for every 30 minutes.

## Database connection details

If you want to use your own db client, use these details

```txt
Database type: PostgreSQL
Host: 143.244.152.246
Post: 443
User: postgres
Password: postgres
Database: postgres
```

Example connection setup with TablePlus client

![tp_connection_details.png](images/tp_connection_details.png)

Example sql query with TablePlus client

![tp_sql_query.png](images/tp_sql_query.png)

## Webapp SQL Editor details

Using CloudBeaver: <http://143.244.152.246>

Use the following credentials to login

```txt
Username: postgres
Password: postgres
```

Enable the SQL Editor

![cb_sql_editor.png](images/cb_sql_editor.png)

Select the db connection

![cb_connection.png](images/cb_connection.png)

Create your query and run it

![cb_sql_query.png](images/cb_sql_query.png)
